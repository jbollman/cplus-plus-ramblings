#include <iostream>
#include <string>
#include <vector>
#include <sstream>

using namespace std;

int main()
{
  
    vector<string> sentences;
    const string studentData[] = 
    { "A1,John,Smith,John1989@gmail.com,20,30,35,40,SECURITY",
      "A2,Suzan,Erickson,Erickson_1990@gmailcom,19,50,30,40,NETWORK",
      "A3,Jack,Napoli,The_lawyer99yahoo.com,19,20,40,33,SOFTWARE",
      "A4,Erin,Black,Erin.black@comcast.net,22,50,58,40,SECURITY",
      "A5,Joseph,Bollman,jbollm1@wgu.edu,28,3,7,13,SOFTWARE"
    };

    for (auto student : studentData)
    {
        sentences.push_back(student);
    }
    vector<string> results;

    for (auto words : sentences) {
        stringstream  ss(words);
        string str;
        while (getline(ss, str, ',')) {
            results.push_back(str);
        }
    }
    int myint2 = std::stoi(results[7]);
    cout << myint2 << endl;
    std::string str1 = "45";
    int myint1 = std::stoi(str1);
    std::cout << "std::stoi(\"" << str1 << "\") is " << myint1 << '\n';
    //display result
    for (auto word : results) {
        cout << word << '\n';
    }
    //b = (int) a;    // c-like cast notation
    //b = int (a);    // functional notation 

    return 0;
}